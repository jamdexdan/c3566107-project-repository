using Microsoft.VisualStudio.TestTools.UnitTesting;
using c3566107_Project; // This is the namespace from the actual project code 
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;

namespace c3566107_project_test
{
    [TestClass]
    public class TestingClass
    {
        [TestMethod]
        private void Form1_Paint(object senderef, PaintEventArgs e)
        {
            Graphics g = e.Graphics;
            Pen p = new Pen(Color.Black); // Will paint exterior
            Brush b = new SolidBrush(Color.Blue); // Will paint interior

            g.DrawLine(p, 0, 0, 100, 100); // The first 2 values is the starting coordinate (x and y) and the 3rd/4th are the end coordinates (x and y)
            g.DrawRectangle(p, 100, 100, 200, 150); //the first 2 values are x and y coordinates (1 and 2), the third value is width and fourth height
            g.DrawEllipse(p, 300, 200, 50, 50); //Works the same way as the rectangle
            g.DrawArc(p, 400, 100, 50, 50, 0, 90); // The first 2 numbers are the starting co ordinates (x,y), the next 2 numbers are the area (width and height) and the last 2 are the starting degree and end degree.
        }
    }
}
